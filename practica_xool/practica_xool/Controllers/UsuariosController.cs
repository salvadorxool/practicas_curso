﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace practica_xool.Controllers
{
   // [Route("[controller]")] puede ser el como se llamara su ruta de acceso en este caso controller= al nombre del controlador(usuarios)
    public class UsuariosController : Controller
        //modo de depuracion sirve para ver como trabaja el codigo y como se va ejecutando parte por parte
    {
        //[HttpGet]
        //[Route("/Usuarios/Xool")]
       // [HttpGet("[controller]/[action]/{data:double}")]//funciona de la manera que sea el nombre del controlador, la accion que se ejecuta y el tipo de dato que es el parametro que recibira para mostrar en la pagina
        public IActionResult Index(double data)//valor que se utilizara
        {
            //var url = Url.Action("Metodo","Usuarios",new {age=23,name="Salvador"});//Url personalizada 
            // return View("Index", data);//nombre de la vista(accion) mas el valor que tendra o estara acompañado

            var url = Url.RouteUrl("Salvador", new { age = 23, name = "Salvador" });// otra manera de como usar las variables para poder usarlas en el accion y se visualice en la web

            return Redirect(url);   //redirect sirve para redirigir el url asignado como accion el Metodo 
        }
        [HttpGet("[controller]/[action]", Name = "Salvador")]
        public IActionResult Metodo(int age, String name)//para agregar vista(del metodo), click derecho y agregar vista
        {
            var data = $"nombre {name} edad {age}";
            return View("Index", data);
        }
    }
}
